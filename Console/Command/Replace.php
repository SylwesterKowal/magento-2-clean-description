<?php


namespace Kowal\Parser\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Replace extends Command
{
    const STORE_ID = "store";
    const SKU = "sku";

    const BR_OPTION = "br";
    const STYLE_OPTION = "style";
    const SCRIPT_OPTION = "script";
    const HELP = "help";

    /**
     * @var \Kowal\Parser\Lib\MagentoService
     */
    protected $magentoService;


    protected $store_id = 0;
    protected $test_sku = false;

    /**
     * Przelicz constructor.
     * @param \Kowal\Parser\Lib\MagentoService $magentoService
     */
    public function __construct(
        \Kowal\Parser\Lib\MagentoService $magentoService
    )
    {
        parent::__construct();
        $this->magentoService = $magentoService;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        if (!$this->store_id = $input->getArgument(self::STORE_ID)) {
            $this->store_id = 0;
        }
        if (!$this->test_sku = $input->getArgument(self::SKU)) {
            $this->test_sku = false;
        }
        $c = $input->getOption(self::STYLE_OPTION);
        $j = $input->getOption(self::SCRIPT_OPTION);
        $b = $input->getOption(self::BR_OPTION);
        $h = $input->getOption(self::HELP);

        if (empty($c) && empty($j) && empty($b) || $h == "-h") {
            $output->writeln("Polecenie: kowal_parser:replace [store_id] [-c clear STYLE ] [-j celar script ] [-b clear BR ]");
            return;
        }

        $output->writeln("Start ");
        $products = $this->getProductDescription();
        foreach ($products as $product) {
            $content = $product['description'];
            if ($content != '') {

                $output->writeln($product['sku']);
                if ($c) $content = $this->cleanStyle($content);
                if ($j) $content = $this->cleanScript($content);
                if ($b) $content = $this->cleanBr($content);

                if ($this->test_sku) {
                    $output->writeln($content);
                    break;
                }

                $this->magentoService
                    ->setStoreID($this->store_id)
                    ->updateAtt($product['sku'], $content, 'description', 'catalog_product_entity_text');

            }
        }
        $output->writeln('Koniec');
    }


    function cleanBr($body)
    {
        $html = str_replace("<br>", '', $body);
        $html = str_replace("<br/>", '', $html);
        $html = str_replace("<br />", '', $html);
        $html = str_replace("<BR />", '', $html);
        $html = str_replace("<BR/>", '', $html);
        $html = str_replace("<BR>", '', $html);
        return $html;
    }

    function cleanScript($body)
    {
        $html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $body);
        return $html;
    }

    function cleanStyle($body)
    {
        $html = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $body);
        $html = preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#', '\\1\\6', $html);;
        return $html;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_parser:replace");
        $this->setDescription("Czyszczenie html");
        $this->setDefinition([
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "Podaj STORE ID"),
            new InputArgument(self::SKU, InputArgument::OPTIONAL, "Podaj SKU"),
            new InputOption(self::BR_OPTION, "-b", InputOption::VALUE_NONE, "Usuń br"),
            new InputOption(self::STYLE_OPTION, "-c", InputOption::VALUE_NONE, "Usuń style"),
            new InputOption(self::SCRIPT_OPTION, "-j", InputOption::VALUE_NONE, "Usuń script"),
            new InputOption(self::HELP, "-h", InputOption::VALUE_NONE, "Usuń script")
        ]);
        parent::configure();
    }

    protected function getProductDescription()
    {
        $sku = ($this->test_sku) ? " AND catalog_product_entity.sku = '{$this->test_sku}'" : "";
        $query = "
        SELECT catalog_product_entity.entity_id,
               catalog_product_entity.sku,
               nametable.store_id,
               nametable.value AS description
        FROM `catalog_product_entity_text` AS nametable
                 LEFT JOIN catalog_product_entity
                           ON nametable.entity_id = catalog_product_entity.entity_id
        
        WHERE nametable.attribute_id = (SELECT attribute_id
                                        FROM `eav_attribute`
                                        WHERE `entity_type_id` = 4
                                          AND `attribute_code` LIKE 'description' )
        AND nametable.store_id = {$this->store_id} {$sku}";
        $connection = $this->magentoService->_getReadConnection();

        return $connection->fetchAll($query);
    }
}
